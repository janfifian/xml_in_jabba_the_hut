package data.informacje;

import org.simpleframework.xml.*;

@Root(name="autor")
public class Autor {
	@Attribute(name="semestr")
	private int semestr;
	@Attribute(name="rejestracja")
	private String rejestracja;
	@Element(name="dane_osobowe")
	private DaneOsobowe daneOsobowe;
	@Element(name="lista_indeks�w")
	private ListaIndeksow listaIndeksow;
	public int getSemestr() {
		return semestr;
	}
	public void setSemestr(int semestr) {
		this.semestr = semestr;
	}
	public String getRejestracja() {
		return rejestracja;
	}
	public void setRejestracja(String rejestracja) {
		this.rejestracja = rejestracja;
	}
	public DaneOsobowe getDaneOsobowe() {
		return daneOsobowe;
	}
	public void setDaneOsobowe(DaneOsobowe daneOsobowe) {
		this.daneOsobowe = daneOsobowe;
	}
	public ListaIndeksow getListaIndeksow() {
		return listaIndeksow;
	}
	public void setListaIndeksow(ListaIndeksow listaIndeksow) {
		this.listaIndeksow = listaIndeksow;
	}
}
