package data.informacje;

import org.simpleframework.xml.*;
@Root(name="dane_osobowe")
public class DaneOsobowe {

	@Element(name="imi�")
	private String imie;
	@Element(name="nazwisko")
	private String nazwisko;
	@Element(name="tytu�_naukowy", required=false)
	private String tytul_naukowy;
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getTytul_naukowy() {
		return tytul_naukowy;
	}
	public void setTytul_naukowy(String tytul_naukowy) {
		this.tytul_naukowy = tytul_naukowy;
	}
}
