package data.informacje;

import java.util.List;

import org.simpleframework.xml.*;


@Root(name="informacje")
public class Informacje {

	@ElementList(name = "autor",inline = true)
	private List<Autor> lista_autorow;

	public List<Autor> getLista_autorow() {
		return lista_autorow;
	}

	public void setLista_autorow(List<Autor> lista_autorow) {
		this.lista_autorow = lista_autorow;
	} 
}
