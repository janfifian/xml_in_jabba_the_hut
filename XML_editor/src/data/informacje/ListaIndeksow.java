package data.informacje;

import java.util.List;

import org.simpleframework.xml.*;
@Root(name="lista_indeks�w")
public class ListaIndeksow {
	
	@ElementList(name = "indeks",inline = true)
	private List<Indeks> lista_indeksow;

	public List<Indeks> getLista_indeksow() {
		return lista_indeksow;
	}

	public void setLista_indeksow(List<Indeks> lista_indeksow) {
		this.lista_indeksow = lista_indeksow;
	} 

}
