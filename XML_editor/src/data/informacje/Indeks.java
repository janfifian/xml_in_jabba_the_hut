package data.informacje;

import org.simpleframework.xml.*;

@Root(name="indeks")
public class Indeks {
	@Attribute(name="aktualny", required=false)
	private String aktualny;
	@Text
	private String index;
	public String getAktualny() {
		return aktualny;
	}
	public void setAktualny(String aktualny) {
		this.aktualny = aktualny;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
}
