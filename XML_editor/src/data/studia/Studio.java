package data.studia;

import org.simpleframework.xml.*;

@Root(name="studio")
public class Studio {

	@Attribute(name="id")
	private String id;
	@Element(name="nazwa")
	private String nazwa;
	@Element(name="lokalizacja")
	private Lokalizacja lokalizacja;
	@Element(name="opis")
	private String opis;
	@Element(name="data_za�o�enia")
	private String data_zalozenia;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public Lokalizacja getLokalizacja() {
		return lokalizacja;
	}
	public void setLokalizacja(Lokalizacja lokalizacja) {
		this.lokalizacja = lokalizacja;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getData_zalozenia() {
		return data_zalozenia;
	}
	public void setData_zalozenia(String data_zalozenia) {
		this.data_zalozenia = data_zalozenia;
	}
	
}
