package data.studia;

import org.simpleframework.xml.*;

@Root(name="lokalizacja")
public class Lokalizacja {
	@Element(name="stan", required=false)
	private String stan;
	@Element(name="kraj")
	private String kraj;
	public String getStan() {
		return stan;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	public String getKraj() {
		return kraj;
	}
	public void setKraj(String kraj) {
		this.kraj = kraj;
	}
}
