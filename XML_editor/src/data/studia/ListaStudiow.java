package data.studia;

import java.util.List;

import org.simpleframework.xml.*;

@Root(name="lista_studi�w")
public class ListaStudiow {

	@ElementList(name = "studio",inline = true)
	private List<Studio> Studia;

	public List<Studio> getStudia() {
		return Studia;
	}

	public void setStudia(List<Studio> studia) {
		Studia = studia;
	}
	
	public boolean checkIdUniqueness(String id) {
		for(Studio studio : Studia) {
			if(studio.getId().equals(id)) return false;
		}
		return true;
	}
}
