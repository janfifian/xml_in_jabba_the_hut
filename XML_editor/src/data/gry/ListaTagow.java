package data.gry;

import java.util.List;

import org.simpleframework.xml.*;


@Root(name="lista_tag�w")
public class ListaTagow {

	@ElementList(name = "tag",inline = true)
	private List<Tag> tagi;

	public List<Tag> getTagi() {
		return tagi;
	}

	public void setTagi(List<Tag> tagi) {
		this.tagi = tagi;
	}
}
