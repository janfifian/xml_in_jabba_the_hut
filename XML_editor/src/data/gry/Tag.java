package data.gry;

import org.simpleframework.xml.*;

@Root(name="tag")
public class Tag {

	@Text
	private String Tag;

	public String getTag() {
		return Tag;
	}

	public void setTag(String tag) {
		Tag = tag;
	}
}
