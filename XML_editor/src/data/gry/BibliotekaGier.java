package data.gry;

import java.util.List;

import org.simpleframework.xml.*;

@Root(name="biblioteka_gier")
public class BibliotekaGier {

	@ElementList(name = "gra",inline = true)
	private List<Gra> gry;

	public List<Gra> getGry() {
		return gry;
	}

	public void setGry(List<Gra> gry) {
		this.gry = gry;
	}
	
}
