package data.gry;

import org.simpleframework.xml.*;

import data.studia.Studio;

@Root(name="gra")
public class Gra {

	@Element(name="nazwa")
	private String nazwa;

	@Element(name="studio")
	private Studio studio;
	
	@Element(name="lista_tag�w")
	private ListaTagow listaTagow;
	
	@Element(name="ocena")
	private int ocena;
	
	@Element(name="cena_w_usd")
	private float cenaWUSD;
	
	@Element(name="data_wydania")
	private String dataWydania;
	
	@Element(name="opis_gry")
	private String opisGry;
	
	@Element(name="liczba_sprzedanych_kopii")
	private int liczbaSprzedanychKopii;

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Studio getStudio() {
		return studio;
	}

	public void setStudio(Studio studio) {
		this.studio = studio;
	}

	public ListaTagow getListaTagow() {
		return listaTagow;
	}

	public void setListaTagow(ListaTagow listaTagow) {
		this.listaTagow = listaTagow;
	}

	public int getOcena() {
		return ocena;
	}

	public void setOcena(int ocena) {
		this.ocena = ocena;
	}

	public float getCenaWUSD() {
		return cenaWUSD;
	}

	public void setCenaWUSD(float cenaWUSD) {
		this.cenaWUSD = cenaWUSD;
	}

	public String getDataWydania() {
		return dataWydania;
	}

	public void setDataWydania(String dataWydania) {
		this.dataWydania = dataWydania;
	}

	public String getOpisGry() {
		return opisGry;
	}

	public void setOpisGry(String opisGry) {
		this.opisGry = opisGry;
	}

	public int getLiczbaSprzedanychKopii() {
		return liczbaSprzedanychKopii;
	}

	public void setLiczbaSprzedanychKopii(int liczbaSprzedanychKopii) {
		this.liczbaSprzedanychKopii = liczbaSprzedanychKopii;
	}
}
