package data;

import org.simpleframework.xml.*;

import data.gry.BibliotekaGier;
import data.informacje.Informacje;
import data.studia.ListaStudiow;

@Root(name="groteka")
public class Groteka {
	
	@Element(name="lista_studi�w")
	private ListaStudiow lista_studiow;
	
	@Element(name="biblioteka_gier")
	private BibliotekaGier biblioteka_gier;
	
	@Element(name="informacje")
	private Informacje informacje;

	public ListaStudiow getLista_studiow() {
		return lista_studiow;
	}

	public void setLista_studiow(ListaStudiow lista_studiow) {
		this.lista_studiow = lista_studiow;
	}

	public BibliotekaGier getBiblioteka_gier() {
		return biblioteka_gier;
	}

	public void setBiblioteka_gier(BibliotekaGier biblioteka_gier) {
		this.biblioteka_gier = biblioteka_gier;
	}

	public Informacje getInformacje() {
		return informacje;
	}

	public void setInformacje(Informacje informacje) {
		this.informacje = informacje;
	}


}
