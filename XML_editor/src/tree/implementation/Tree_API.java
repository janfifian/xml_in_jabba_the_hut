package tree.implementation;

import data.Groteka;
import data.gry.*;
import data.informacje.*;
import data.studia.*;
import tree.implementation.gamenodes.BibliotekaGierNode;
import tree.implementation.gamenodes.GraNode;
import tree.implementation.gamenodes.ListaTagowNode;
import tree.implementation.gamenodes.TagNode;
import tree.implementation.informacjenodes.AutorNode;
import tree.implementation.informacjenodes.DaneOsoboweNode;
import tree.implementation.informacjenodes.IndeksNode;
import tree.implementation.informacjenodes.InformacjeNode;
import tree.implementation.informacjenodes.ListaIndeksowNode;
import tree.implementation.studianodes.ListaStudiowNode;
import tree.implementation.studianodes.LokalizacjaNode;
import tree.implementation.studianodes.StudioNode;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

public class Tree_API {

	public static GrotekaNode generateRoot(Groteka groteka) {
		GrotekaNode rootNode = new GrotekaNode(groteka);
		ListaStudiowNode listNode = generateGameStudioSetNode(groteka);
		InformacjeNode infoNode = generateInfoNode(groteka.getInformacje());
		BibliotekaGierNode gamesNode = generateGameLibNode(groteka.getBiblioteka_gier());
		rootNode.add(listNode);
		rootNode.add(gamesNode);
		rootNode.add(infoNode);
		return rootNode;
	}
	
	private static ListaStudiowNode generateGameStudioSetNode(Groteka groteka) {
		ListaStudiowNode listNode = new ListaStudiowNode(groteka.getLista_studiow());
		for(Studio studio : listNode.getListaStudiow().getStudia()) {
			StudioNode node = new StudioNode(studio);
			node.add(generateLokalizacjaNode(studio));
			listNode.add(node);
		}
		return listNode;
	}
	
	private static InformacjeNode generateInfoNode(Informacje info) {
		InformacjeNode infoNode = new InformacjeNode(info);
		for(Autor autor : info.getLista_autorow()) {
			AutorNode autorNode = new AutorNode(autor);
			ListaIndeksowNode indexlistNode = new ListaIndeksowNode(autor.getListaIndeksow());
			/*
			 * Adding indexlist at the same time
			 */
			for(Indeks index : autor.getListaIndeksow().getLista_indeksow()) {
				IndeksNode indexNode = new IndeksNode(index);
				indexlistNode.add(indexNode);
			}
			autorNode.add(indexlistNode);
			/*
			 * Well, why don't we add daneOsoboweNode as well
			 */
			DaneOsoboweNode personalDataNode = new DaneOsoboweNode(autor.getDaneOsobowe());
			autorNode.add(personalDataNode);
			infoNode.add(autorNode);
		}
		
		return infoNode;
	}
	
	private static BibliotekaGierNode generateGameLibNode(BibliotekaGier bib) {
		BibliotekaGierNode bibNode = new BibliotekaGierNode(bib);
		for(Gra gra : bib.getGry()) {
			GraNode gameNode = new GraNode(gra);
			ListaTagowNode taglistNode = new ListaTagowNode(gra.getListaTagow());
			for(Tag tag : gra.getListaTagow().getTagi()) {
				TagNode tagNode = new TagNode(tag);
				taglistNode.add(tagNode);
			}
			gameNode.add(taglistNode);
			bibNode.add(gameNode);
		}
		return bibNode;
	}
	
	private static LokalizacjaNode generateLokalizacjaNode(Studio st) {
		LokalizacjaNode node = new LokalizacjaNode(st.getLokalizacja());
		return node;
	}
}
