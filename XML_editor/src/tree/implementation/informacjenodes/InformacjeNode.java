package tree.implementation.informacjenodes;

import javax.swing.tree.DefaultMutableTreeNode;

import data.informacje.Informacje;

public class InformacjeNode extends DefaultMutableTreeNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Informacje info;

	public InformacjeNode(Informacje info) {
		super("Informacje o Autorach");
		this.info = info;
	}
	
	public Informacje getInfo() {
		return info;
	}

	public void setInfo(Informacje info) {
		this.info = info;
	}
	
}
