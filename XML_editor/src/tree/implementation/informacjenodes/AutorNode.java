package tree.implementation.informacjenodes;

import javax.swing.tree.DefaultMutableTreeNode;

import data.informacje.Autor;

public class AutorNode extends DefaultMutableTreeNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Autor autor;
	public AutorNode(Autor autor){
		super("Autor: "+autor.getDaneOsobowe().getImie()+" "+autor.getDaneOsobowe().getNazwisko());
		this.autor = autor;
	}
	
	public Autor getAutor() {
		return autor;
	}
	public void setAutor(Autor autor) {
		this.autor = autor;
	}
	
}
