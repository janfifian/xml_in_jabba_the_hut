package tree.implementation.informacjenodes;

import javax.swing.tree.DefaultMutableTreeNode;

import data.informacje.ListaIndeksow;

public class ListaIndeksowNode extends DefaultMutableTreeNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ListaIndeksow indexlist;
	
	public ListaIndeksowNode(ListaIndeksow indexlist) {
		super("Lista indeks�w");
		this.indexlist=indexlist;
	}

	public ListaIndeksow getIndexlist() {
		return indexlist;
	}

	public void setIndexlist(ListaIndeksow indexlist) {
		this.indexlist = indexlist;
	}
	
}
