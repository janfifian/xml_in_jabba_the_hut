package tree.implementation.informacjenodes;

import javax.swing.tree.DefaultMutableTreeNode;

import data.informacje.Indeks;

public class IndeksNode extends DefaultMutableTreeNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Indeks index;

	public IndeksNode(Indeks index) {
		super(index.getIndex());
		this.index=index;
	}
	
	public Indeks getIndex() {
		return index;
	}

	public void setIndex(Indeks index) {
		this.index = index;
	}
	
}
