package tree.implementation.informacjenodes;

import javax.swing.tree.DefaultMutableTreeNode;

import data.informacje.DaneOsobowe;

public class DaneOsoboweNode extends DefaultMutableTreeNode {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DaneOsobowe daneOsobowe;
	public DaneOsoboweNode(DaneOsobowe daneOsobowe){
		super(daneOsobowe.getTytul_naukowy()!=null?
				daneOsobowe.getTytul_naukowy()+" "+daneOsobowe.getImie()+" "+daneOsobowe.getNazwisko():
					daneOsobowe.getImie()+" "+daneOsobowe.getNazwisko()
				);
		this.daneOsobowe = daneOsobowe;
	}
	public DaneOsobowe getDaneOsobowe() {
		return daneOsobowe;
	}
	public void setDaneOsobowe(DaneOsobowe daneOsobowe) {
		this.daneOsobowe = daneOsobowe;
	}
}
