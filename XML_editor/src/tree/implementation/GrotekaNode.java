package tree.implementation;

import data.Groteka;
import data.gry.Gra;

public class GrotekaNode extends javax.swing.tree.DefaultMutableTreeNode {

	private Groteka groteka;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GrotekaNode(Groteka groteka){
		super("Groteka");
		this.groteka = groteka;
	}

	public Groteka getGroteka() {
		return groteka;
	}

	public void setGroteka(Groteka groteka) {
		this.groteka = groteka;
	}
	
}
