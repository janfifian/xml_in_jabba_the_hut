package tree.implementation.studianodes;
import data.studia.Lokalizacja;

public class LokalizacjaNode extends javax.swing.tree.DefaultMutableTreeNode {

	private Lokalizacja lokalizacja;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LokalizacjaNode(Lokalizacja lokalizacja){
		super("Lokalizacja: "+(lokalizacja.getStan()!=null? lokalizacja.getStan()+", "+lokalizacja.getKraj() : lokalizacja.getKraj()));
		this.lokalizacja = lokalizacja;
	}

	public Lokalizacja getLokalizacja() {
		return lokalizacja;
	}

	public void setLokalizacja(Lokalizacja lokalizacja) {
		this.lokalizacja = lokalizacja;
	}
}
