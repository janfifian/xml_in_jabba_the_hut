package tree.implementation.studianodes;

import data.studia.ListaStudiow;

public class ListaStudiowNode extends javax.swing.tree.DefaultMutableTreeNode {

	private ListaStudiow listaStudiow;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ListaStudiowNode(ListaStudiow listaStudiow){
		super("Lista Studi�w");
		this.listaStudiow = listaStudiow;
	}

	public ListaStudiow getListaStudiow() {
		return listaStudiow;
	}

	public void setListaStudiow(ListaStudiow listaStudiow) {
		this.listaStudiow = listaStudiow;
	}
	
}
