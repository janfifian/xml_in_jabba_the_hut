package tree.implementation.studianodes;
import data.studia.Studio;

public class StudioNode extends javax.swing.tree.DefaultMutableTreeNode {

	private Studio studio;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StudioNode(Studio studio){
		super("Studio "+studio.getNazwa());
		this.studio = studio;
	}

	public Studio getStudio() {
		return studio;
	}

	public void Studio(Studio studio) {
		this.studio = studio;
	}
	
}
