package tree.implementation.gamenodes;

import data.gry.Gra;

public class GraNode extends javax.swing.tree.DefaultMutableTreeNode {

	private Gra gra;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GraNode(Gra gra){
		super(gra.getNazwa());
		this.gra = gra;
	}

	public Gra getGra() {
		return gra;
	}

	public void setGra(Gra gra) {
		this.gra = gra;
	}
	
}
