package tree.implementation.gamenodes;

import data.gry.BibliotekaGier;

public class BibliotekaGierNode extends javax.swing.tree.DefaultMutableTreeNode {

	private BibliotekaGier groteka;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BibliotekaGierNode(BibliotekaGier groteka){
		super("Biblioteka Gier");
		this.groteka = groteka;
	}

	public BibliotekaGier getBibliotekaGier() {
		return groteka;
	}

	public void setBibliotekaGier(BibliotekaGier groteka) {
		this.groteka = groteka;
	}
	
}

