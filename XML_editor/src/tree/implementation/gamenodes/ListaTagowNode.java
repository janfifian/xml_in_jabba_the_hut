package tree.implementation.gamenodes;

import data.gry.ListaTagow;

public class ListaTagowNode extends javax.swing.tree.DefaultMutableTreeNode {

	private ListaTagow taglist;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ListaTagowNode(ListaTagow taglist){
		super("Lista Tag�w");
		this.taglist = taglist;
	}

	public ListaTagow getListaTagow() {
		return taglist;
	}

	public void setListaTagow(ListaTagow taglist) {
		this.taglist = taglist;
	}
}
