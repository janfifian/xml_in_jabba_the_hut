package tree.implementation.gamenodes;

import data.gry.Tag;

public class TagNode extends javax.swing.tree.DefaultMutableTreeNode {

	private Tag tag;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TagNode(Tag tag){
		super(tag.getTag());
		this.tag = tag;
	}

	public Tag getTag() {
		return tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}
	
}
