package gui;


import logic.HTMLConverter;
import logic.XMLSaver;
import tree.implementation.Tree_API;
import tree.implementation.gamenodes.GraNode;
import tree.implementation.gamenodes.TagNode;
import tree.implementation.informacjenodes.AutorNode;
import tree.implementation.informacjenodes.DaneOsoboweNode;
import tree.implementation.informacjenodes.IndeksNode;
import tree.implementation.studianodes.LokalizacjaNode;
import tree.implementation.studianodes.StudioNode;

import java.io.File;

import java.util.regex.Pattern;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import data.Groteka;
import data.gry.*;
import data.informacje.*;
import data.studia.*;
import gui.SeparateWindows.AuthorRegistrationModifyObject;
import gui.SeparateWindows.GameModifierWindow;
import gui.SeparateWindows.IndexModifyObject;
import gui.SeparateWindows.LokacjaModifyObject;
import gui.SeparateWindows.PersonalDataModifyObject;
import gui.SeparateWindows.StudioModifyObject;
import gui.SeparateWindows.TagModifyObject;

import javax.swing.JTree;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import javax.swing.JTable;


public class Main_Window {
	private static JScrollPane tableView;
	private static DefaultTreeModel treeModel;
	private static JTree tree;
	private Groteka groteka;
	private String pattern = Pattern.quote(System.getProperty("file.separator"));
	private String_Joiner joiner;
	private JFrame frmXmlGrotekaEditor;
	
	private XMLSaver saver;
	private HTMLConverter converter;
	private static Serializer serializer = new Persister();

	private File XMLFile;
	private File XSLTFile;
	private JTable advancedView;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main_Window window = new Main_Window();
					window.frmXmlGrotekaEditor.setName("XML Groteka Editor");
					window.frmXmlGrotekaEditor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main_Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {		
		//initialize table model
		String[] columnNames = {"Node Name", "Node Type", "Node Value"};
		Object[][] data= {{"","",""}};
		TableModel tableModel = new DefaultTableModel(data,columnNames) {
		    /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		tableModel.addTableModelListener(new ChangeListener());
		
		//initialize gui helpers
		joiner = new String_Joiner();
		
		//initializing logic components
		saver = new XMLSaver();
		converter = new HTMLConverter();
		
		//initializing modifiableinfo
		JTextPane textPaneModifiableInfo = new JTextPane();
		textPaneModifiableInfo.setBounds(312, 444, 180, 20);
		textPaneModifiableInfo.setText("No.");
		textPaneModifiableInfo.setEditable(false);
		textPaneModifiableInfo.setDisabledTextColor(new Color(200,20,20));
		
		
		frmXmlGrotekaEditor = new JFrame();
		frmXmlGrotekaEditor.setTitle("XML Groteka editor");
		frmXmlGrotekaEditor.setBounds(100, 100, 1000, 700);
		frmXmlGrotekaEditor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmXmlGrotekaEditor.getContentPane().setLayout(null);

		frmXmlGrotekaEditor.getContentPane().add(textPaneModifiableInfo);
		//XML viewer
		DefaultMutableTreeNode rootNode =new DefaultMutableTreeNode("No file provided");
		treeModel = new DefaultTreeModel(rootNode);
		treeModel.addTreeModelListener(new tree.implementation.Hearer());
		/*
		 * Instruct the table what to draw
		 * in case when particular node is selected.
		 */
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(182, 11, 673, 360);
		frmXmlGrotekaEditor.getContentPane().add(scrollPane);
		tree = new JTree(treeModel);
		scrollPane.setViewportView(tree);
		tree.setEditable(false);
		tree.getSelectionModel().setSelectionMode
		        (TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setShowsRootHandles(true);
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                tree.getLastSelectedPathComponent();
				if (node == null) { 
					((DefaultTableModel) tableModel).setRowCount(0);
					return;}
				else {
					if(node.getClass().getName() == "tree.implementation.gamenodes.BibliotekaGierNode" ||
							node.getClass().getName() == "tree.implementation.GrotekaNode" ||
							node.getClass().getName() == "tree.implementation.gamenodes.ListaTagowNode" ||
							node.getClass().getName() == "tree.implementation.informacjenodes.InformacjeNode"||
							node.getClass().getName() == "tree.implementation.informacjenodes.ListaIndeksowNode"||
							node.getClass().getName() == "tree.implementation.studianodes.ListaStudiowNode") {
						textPaneModifiableInfo.setText("No.");
					}
					else {
						textPaneModifiableInfo.setText("Yes.");
					}
					TableViewerHandler.HandleNode(node,tableModel);
					
				}
			}
		});
		
		//HTML_converting_section
		JButton btnConverttohtml = new JButton("Convert_to_HTML");
		btnConverttohtml.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
			    chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
			    if(chooser.showSaveDialog(null)==JFileChooser.APPROVE_OPTION) {
			    	try {
			    		converter.convertToHTML(XMLFile,XSLTFile,chooser.getSelectedFile());		
			        } catch (Exception ex) {
			            ex.printStackTrace();
			        }		
			    }
			}
		});
		btnConverttohtml.setBounds(10, 615, 150, 23);
		frmXmlGrotekaEditor.getContentPane().add(btnConverttohtml);
		
		
		//File_saving section
		JButton btnSaveButton = new JButton("Save");
		btnSaveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
			    chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
			    if(chooser.showSaveDialog(null)==JFileChooser.APPROVE_OPTION) {
			    	try {
			    		saver.saveToXML(groteka, chooser.getSelectedFile());		
			        } catch (Exception ex) {
			            ex.printStackTrace();
			        }	    	
			    }
			}
		});
		btnSaveButton.setBounds(10, 581, 150, 23);
		frmXmlGrotekaEditor.getContentPane().add(btnSaveButton);
		
		//XML Choice
		JTextPane textPaneXML = new JTextPane();
		textPaneXML.setBounds(10, 110, 150, 95);
		textPaneXML.setEditable(false);
		frmXmlGrotekaEditor.getContentPane().add(textPaneXML);
		
		JButton btnOpenXml = new JButton("Open XML");
		btnOpenXml.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
			    chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
			    if(chooser.showSaveDialog(null)==JFileChooser.APPROVE_OPTION) {
			    	try {
			    		XMLFile = chooser.getSelectedFile();
			    		groteka = serializer.read(Groteka.class, XMLFile);
			    		String[] textToSet = XMLFile.toString().split(pattern);
			    		textPaneXML.setText(joiner.textToSet(textToSet, "\\ "));
			    		tree.setRootVisible(false);
			    		if(((MutableTreeNode) treeModel.getRoot()).getChildCount()>0) {
			    			treeModel.removeNodeFromParent((MutableTreeNode) ((MutableTreeNode) treeModel.getRoot()).getChildAt(0));
			    		}
			    		treeModel.insertNodeInto(Tree_API.generateRoot(groteka),(MutableTreeNode) treeModel.getRoot(), 0);
			    		treeModel.reload();
			    	} catch (Exception ex) {
			            ex.printStackTrace();
			        }	    	
			    }
			}
		});
		btnOpenXml.setBounds(10, 50, 150, 23);
		frmXmlGrotekaEditor.getContentPane().add(btnOpenXml);
		
		JLabel lblCurrentlyChosenXml = new JLabel("Currently chosen XML file:");
		lblCurrentlyChosenXml.setBounds(10, 84, 150, 14);
		frmXmlGrotekaEditor.getContentPane().add(lblCurrentlyChosenXml);
		
		
		//XSLT Choice
		JTextPane textPaneXSLT = new JTextPane();
		textPaneXSLT.setBounds(10, 276, 150, 95);
		textPaneXSLT.setEditable(false);
		frmXmlGrotekaEditor.getContentPane().add(textPaneXSLT);
		
		JLabel label = new JLabel("Currently chosen XSLT file:");
		label.setBounds(10, 250, 150, 14);
		frmXmlGrotekaEditor.getContentPane().add(label);
		
		JButton btnOpenXSLT = new JButton("Open XSLT");
		btnOpenXSLT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
			    chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
			    if(chooser.showSaveDialog(null)==JFileChooser.APPROVE_OPTION) {
			    	try {
			    		XSLTFile = chooser.getSelectedFile();
			    		String[] textToSet = XSLTFile.toString().split(pattern);
			    		textPaneXSLT.setText(joiner.textToSet(textToSet, "\\ "));
			        } catch (Exception ex) {
			            ex.printStackTrace();
			        }	    	
			    }
			}
		});
		btnOpenXSLT.setBounds(10, 216, 150, 23);
		frmXmlGrotekaEditor.getContentPane().add(btnOpenXSLT);
		
		JLabel lblIsThisNode = new JLabel("Is this node modifiable?");
		lblIsThisNode.setBounds(182, 450, 120, 14);
		frmXmlGrotekaEditor.getContentPane().add(lblIsThisNode);
		

		
		//XML Editor tools
		JButton btnAddNewEntry = new JButton("Add new entry");
		btnAddNewEntry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gui.SeparateWindows.GameModifierWindow.GameWindow(treeModel, groteka);
			}
		});
		btnAddNewEntry.setBounds(182, 400, 120, 23);
		frmXmlGrotekaEditor.getContentPane().add(btnAddNewEntry);
		/*
		 *This gonna be a tough one - modifier
		 */
		JButton btnModifyEntry = new JButton("Modify current entry");
		btnModifyEntry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TreePath path = tree.getSelectionPath();
				if(path!=null) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode) 
                            path.getLastPathComponent();
					if(node.getClass().getName() == "tree.implementation.gamenodes.BibliotekaGierNode" ||
							node.getClass().getName() == "tree.implementation.GrotekaNode" ||
							node.getClass().getName() == "tree.implementation.gamenodes.ListaTagowNode" ||
							node.getClass().getName() == "tree.implementation.informacjenodes.InformacjeNode"||
							node.getClass().getName() == "tree.implementation.informacjenodes.ListaIndeksowNode"||
							node.getClass().getName() == "tree.implementation.studianodes.ListaStudiowNode") {
						JOptionPane.showMessageDialog(frmXmlGrotekaEditor,
							    "Immutable item was selected!",
							    "Error",
							    JOptionPane.ERROR_MESSAGE);
					}
					else {
						if(node.getClass().getName() == "tree.implementation.informacjenodes.IndeksNode") {
							IndexModifyObject.ModifyWindow((IndeksNode) node,treeModel);
						}
						if(node.getClass().getName() == "tree.implementation.gamenodes.TagNode") {
							TagModifyObject.ModifyWindow((TagNode) node,treeModel);
						}
						if(node.getClass().getName() == "tree.implementation.informacjenodes.DaneOsoboweNode") {
							PersonalDataModifyObject.ModifyWindow((DaneOsoboweNode) node,treeModel);
						}
						if(node.getClass().getName() == "tree.implementation.informacjenodes.AutorNode") {
							AuthorRegistrationModifyObject.ModifyWindow((AutorNode) node, treeModel);
						}
						if(node.getClass().getName() == "tree.implementation.studianodes.LokalizacjaNode") {
							LokacjaModifyObject.ModifyWindow((LokalizacjaNode) node, treeModel);
						}
						if(node.getClass().getName() == "tree.implementation.studianodes.StudioNode") {
							StudioModifyObject.ModifyWindow((StudioNode) node, treeModel);
						}
					}
				}
				else {
					JOptionPane.showMessageDialog(frmXmlGrotekaEditor,
						    "No item was selected!",
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
				
			}
		});
		btnModifyEntry.setBounds(312, 400, 180, 23);
		frmXmlGrotekaEditor.getContentPane().add(btnModifyEntry);
		
		JButton btnDeleteCurrentEntry = new JButton("Delete current entry");
		btnDeleteCurrentEntry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TreePath path = tree.getSelectionPath();
				if(path!=null) {
					DefaultMutableTreeNode node = (DefaultMutableTreeNode) 
                            path.getLastPathComponent();
					
					if(node.getClass().getName() == "tree.implementation.gamenodes.BibliotekaGierNode" ||
					node.getClass().getName() == "tree.implementation.GrotekaNode" ||
					node.getClass().getName() == "tree.implementation.gamenodes.ListaTagowNode" ||
					node.getClass().getName() == "tree.implementation.informacjenodes.InformacjeNode" ||
					node.getClass().getName() == "tree.implementation.informacjenodes.DaneOsoboweNode" ||
					node.getClass().getName() == "tree.implementation.informacjenodes.ListaIndeksowNode" ||
					node.getClass().getName() == "tree.implementation.studianodes.ListaStudiowNode" ||
					node.getClass().getName() == "tree.implementation.studianodes.LokalizacjaNode") {
						JOptionPane.showMessageDialog(frmXmlGrotekaEditor,
							    "This node cannot be removed!",
							    "Error",
							    JOptionPane.ERROR_MESSAGE);
					}
					else 
					{
					if(node.getClass().getName() == "tree.implementation.gamenodes.GraNode") {
						groteka.getBiblioteka_gier().getGry().remove(((GraNode) node).getGra());
					}
					if(node.getClass().getName() == "tree.implementation.gamenodes.TagNode") {
						Gra gra = ((GraNode) node.getParent().getParent()).getGra();
						gra.getListaTagow().getTagi().remove(((TagNode) node).getTag());
					}
					if(node.getClass().getName() == "tree.implementation.informacjenodes.Autor") {
						groteka.getInformacje().getLista_autorow().remove( ((AutorNode) node).getAutor() );
					}
					if(node.getClass().getName() == "tree.implementation.informacjenodes.IndeksNode") {
						Autor aut = ((AutorNode) node.getParent().getParent()).getAutor();
						aut.getListaIndeksow().getLista_indeksow().remove( ((IndeksNode) node).getIndex() );
					}
					if(node.getClass().getName() == "tree.implementation.studianodes.StudioNode") {
						groteka.getLista_studiow().getStudia().remove(((StudioNode) node).getStudio() );
					}
					treeModel.removeNodeFromParent(node);
					}
				}
				else {
					JOptionPane.showMessageDialog(frmXmlGrotekaEditor,
						    "No item was selected!",
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnDeleteCurrentEntry.setBounds(502, 400, 150, 23);
		frmXmlGrotekaEditor.getContentPane().add(btnDeleteCurrentEntry);
		
		//Autorzy Info:
		JLabel lblAutorzy = new JLabel("Autorzy:");
		lblAutorzy.setBounds(807, 585, 100, 14);
		frmXmlGrotekaEditor.getContentPane().add(lblAutorzy);
		JLabel lblJanFilipowicz = new JLabel("Jan Filipowicz");
		lblJanFilipowicz.setBounds(807, 602, 100, 14);
		frmXmlGrotekaEditor.getContentPane().add(lblJanFilipowicz);
		JLabel lblFilipTurobo = new JLabel("Filip Turobo\u015B");
		lblFilipTurobo.setBounds(807, 619, 100, 14);
		frmXmlGrotekaEditor.getContentPane().add(lblFilipTurobo);
		
		//Advanced view of current Node:
		JLabel lblCurrentNodeAttributes = new JLabel("Current node attributes and elements");
		lblCurrentNodeAttributes.setBounds(182, 488, 310, 14);
		frmXmlGrotekaEditor.getContentPane().add(lblCurrentNodeAttributes);

		advancedView = new JTable(tableModel);
		advancedView.setBounds(185, 508, 467, 142);
		tableView = new JScrollPane(advancedView);
		tableView.setBounds(185, 508, 467, 142);
		frmXmlGrotekaEditor.getContentPane().add(tableView);
		

		//something else
	}
}
