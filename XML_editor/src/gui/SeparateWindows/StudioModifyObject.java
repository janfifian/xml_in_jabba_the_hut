package gui.SeparateWindows;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.tree.TreeModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import data.gry.Tag;
import data.informacje.DaneOsobowe;
import data.informacje.Indeks;
import data.studia.Lokalizacja;
import data.studia.Studio;
import tree.implementation.gamenodes.ListaTagowNode;
import tree.implementation.gamenodes.TagNode;
import tree.implementation.gamenodes.GraNode;
import tree.implementation.informacjenodes.AutorNode;
import tree.implementation.informacjenodes.DaneOsoboweNode;
import tree.implementation.informacjenodes.IndeksNode;
import tree.implementation.informacjenodes.ListaIndeksowNode;
import tree.implementation.studianodes.ListaStudiowNode;
import tree.implementation.studianodes.LokalizacjaNode;
import tree.implementation.studianodes.StudioNode;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

public class StudioModifyObject {
	private static JTextField txtDataZalozenia;
	private static String s;
	private static JTextField txtNazwaStudia;
	private static JTextField txtID;
	private static JTextField txtOpis;
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void ModifyWindow(StudioNode locNode, TreeModel tm) {
		  JFrame frame = new JFrame("Modify Existing Object");
		  frame.addWindowListener(new java.awt.event.WindowAdapter() {
			    @Override
			    public void windowClosed(java.awt.event.WindowEvent windowEvent) {
			    	ListaStudiowNode aut = (ListaStudiowNode) locNode.getParent();
			    	((DefaultTreeModel) tm).removeNodeFromParent(locNode);
			    	Studio stud = locNode.getStudio();
			    	stud.setData_zalozenia(txtDataZalozenia.getText());
			    	stud.setId(txtID.getText());
			    	stud.setNazwa(txtNazwaStudia.getText());
			    	stud.setOpis(txtOpis.getText());
			    	StudioNode stNode = new StudioNode(stud);
			    	stNode.add(locNode.getFirstLeaf());
			    	((DefaultTreeModel) tm).insertNodeInto(stNode,aut,aut.getChildCount() );
			    	}
			});
		  frame.setSize( 550,180 );
		  frame.setLocationRelativeTo( null );
		  frame.getContentPane().setLayout(null);
		  frame.setUndecorated(true);
		  frame.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		  txtDataZalozenia = new JTextField();
		  txtDataZalozenia.setBounds(10, 99, 244, 23);
		  txtDataZalozenia.setText(locNode.getStudio().getData_zalozenia());
		  frame.getContentPane().add(txtDataZalozenia);
		  txtDataZalozenia.setColumns(10);
		  
		  JLabel lblModifyCurrentValue = new JLabel("Modify Current Data Za\u0142o\u017Cenia");
		  lblModifyCurrentValue.setBounds(10, 74, 244, 14);
		  frame.getContentPane().add(lblModifyCurrentValue);
		  
		  JButton btnSave = new JButton("Save!");
		  btnSave.addActionListener(new ActionListener() {
		  	public void actionPerformed(ActionEvent e) {
		  		if(txtNazwaStudia.getText().length()>=1 && txtID.getText().length()>=1) {
		  			if(((ListaStudiowNode) locNode.getParent()).getListaStudiow().checkIdUniqueness(txtID.getText())) {
		  				if(txtDataZalozenia.getText().matches("(19[0-9][0-9]|20[0-1][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|30|31)")) {
		  					frame.dispose();
		  				}
		  				else {
		  					JOptionPane.showMessageDialog(frame,
								    "Improper Data Format!",
								    "Input incorrect",
								    JOptionPane.ERROR_MESSAGE);
		  				}
		  			}
		  			else {
		  				JOptionPane.showMessageDialog(frame,
							    "The given ID is not unique!",
							    "Input incorrect",
							    JOptionPane.ERROR_MESSAGE);
		  			}
		  		}
		  		else {
		  			JOptionPane.showMessageDialog(frame,
						    "No Studio Nazwa or ID detected.",
						    "Input incorrect",
						    JOptionPane.ERROR_MESSAGE);
		  		}
		  	}
		  });
		  btnSave.setBounds(226, 146, 89, 23);
		  frame.getContentPane().add(btnSave);
		  
		  JLabel lblModifyCurrentNazwisko = new JLabel("Modify Current Nazwa Studia");
		  lblModifyCurrentNazwisko.setBounds(10, 15, 151, 14);
		  frame.getContentPane().add(lblModifyCurrentNazwisko);
		  
		  txtNazwaStudia = new JTextField();
		  txtNazwaStudia.setText(locNode.getStudio().getNazwa());
		  txtNazwaStudia.setBounds(10, 40, 244, 23);
		  frame.getContentPane().add(txtNazwaStudia);
		  txtNazwaStudia.setColumns(10);
		  
		  JLabel lblModifyCurrentId = new JLabel("Modify Current ID");
		  lblModifyCurrentId.setBounds(279, 15, 261, 14);
		  frame.getContentPane().add(lblModifyCurrentId);
		  
		  txtID = new JTextField();
		  txtID.setBounds(279, 41, 261, 20);
		  txtID.setText(locNode.getStudio().getId());
		  frame.getContentPane().add(txtID);
		  txtID.setColumns(10);
		  
		  JLabel lblModifyCurrentOpis = new JLabel("Modify Current Opis");
		  lblModifyCurrentOpis.setBounds(279, 74, 261, 14);
		  frame.getContentPane().add(lblModifyCurrentOpis);
		  
		  txtOpis = new JTextField();
		  txtOpis.setBounds(279, 100, 261, 20);
		  txtOpis.setText(locNode.getStudio().getOpis());
		  frame.getContentPane().add(txtOpis);
		  txtOpis.setColumns(10);
		   
		  frame.setVisible(true);
	}
}
