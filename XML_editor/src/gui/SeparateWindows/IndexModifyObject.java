package gui.SeparateWindows;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.tree.TreeModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import data.Groteka;
import data.informacje.Indeks;
import tree.implementation.informacjenodes.AutorNode;
import tree.implementation.informacjenodes.IndeksNode;
import tree.implementation.informacjenodes.ListaIndeksowNode;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class IndexModifyObject {
	private static JTextField textField;
	private static String s;
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void ModifyWindow(IndeksNode index, TreeModel tm) {
		  JFrame frame = new JFrame("Modify Existing Object");
		  JCheckBox chckbxAktualny = new JCheckBox("Aktualny?");
		  chckbxAktualny.setBounds(10, 110, 97, 23);
		  frame.addWindowListener(new java.awt.event.WindowAdapter() {
			    @Override
			    public void windowClosed(java.awt.event.WindowEvent windowEvent) {
			    	ListaIndeksowNode lt = (ListaIndeksowNode) index.getParent();
			    	((DefaultTreeModel) tm).removeNodeFromParent(index);
			    	((AutorNode) lt.getParent()).getAutor().getListaIndeksow().getLista_indeksow().remove(index.getIndex());
			    	Indeks ind = new Indeks();
			    	ind.setIndex(s);
			    	if(chckbxAktualny.isSelected()) {
			    		ind.setAktualny("Tak");
			    	}
			    	else {
			    		ind.setAktualny("Nie");
			    	}
			    	((AutorNode) lt.getParent()).getAutor().getListaIndeksow().getLista_indeksow().add(ind);
			    	((DefaultTreeModel) tm).insertNodeInto(new IndeksNode(ind),lt,lt.getChildCount() );
			    }
			});
		  frame.setSize( 300,200 );
		  frame.setLocationRelativeTo( null );
		  frame.getContentPane().setLayout(null);
		  frame.setUndecorated(true);
		  frame.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		  textField = new JTextField();
		  textField.setBounds(10, 40, 244, 40);
		  textField.setText(index.getIndex().getIndex());
		  frame.getContentPane().add(textField);
		  textField.setColumns(10);
		  
		  JLabel lblModifyCurrentValue = new JLabel("Modify current Indeks value");
		  lblModifyCurrentValue.setBounds(10, 11, 151, 14);
		  frame.getContentPane().add(lblModifyCurrentValue);
		  
		  JButton btnSave = new JButton("Save!");
		  btnSave.addActionListener(new ActionListener() {
		  	public void actionPerformed(ActionEvent e) {
		  		 s = textField.getText();
		  		if(s.matches("[0-9][0-9][0-9][0-9][0-9][0-9]")) {
		  		frame.dispose();
		  		}
		  		else {
		  			JOptionPane.showMessageDialog(frame,
						    "Improper index format.",
						    "Input incorrect",
						    JOptionPane.ERROR_MESSAGE);
		  		}
		  	}
		  });
		  btnSave.setBounds(185, 110, 89, 23);
		  frame.getContentPane().add(btnSave);
		  

		  frame.getContentPane().add(chckbxAktualny);
		  frame.setVisible(true);
	}
}
