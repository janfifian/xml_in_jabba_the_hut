package gui.SeparateWindows;

import javax.swing.JFrame;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import data.informacje.Autor;
import tree.implementation.informacjenodes.AutorNode;
import tree.implementation.informacjenodes.InformacjeNode;

import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JSpinner;

public class AuthorRegistrationModifyObject {
	private static String s;
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void ModifyWindow(AutorNode autNode, TreeModel tm) {
		  JFrame frame = new JFrame("Modify Existing Object");
		  String[] rejestracjePossible = {"Pe�na", "Warunkowa", "Dzieka�ska"};
		  SpinnerModel smstrmodel = new SpinnerNumberModel(6, 1, 10, 1); 
		  JSpinner spinner = new JSpinner(smstrmodel);
		  JComboBox comboBox = new JComboBox(rejestracjePossible);
		  frame.addWindowListener(new java.awt.event.WindowAdapter() {
			    @Override
			    public void windowClosed(java.awt.event.WindowEvent windowEvent) {
				Autor aut = autNode.getAutor();
			    aut.setRejestracja((String) comboBox.getSelectedItem());
			    aut.setSemestr((int) spinner.getValue());
			    InformacjeNode it = (InformacjeNode) autNode.getParent();
			    ((DefaultTreeModel) tm).removeNodeFromParent(autNode);
			    AutorNode newAutorNode = new AutorNode(aut);
			    newAutorNode.add((MutableTreeNode) autNode.getFirstChild());
			    newAutorNode.add(autNode.getLastLeaf());
			    ((DefaultTreeModel) tm).insertNodeInto(newAutorNode,it,it.getChildCount());
			    }
			});
		  frame.setSize( 300,140 );
		  frame.setLocationRelativeTo( null );
		  frame.getContentPane().setLayout(null);
		  frame.setUndecorated(true);
		  frame.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		  
		  JLabel lblModifyCurrentValue = new JLabel("Modify current Rejestracja value");
		  lblModifyCurrentValue.setBounds(10, 11, 280, 14);
		  frame.getContentPane().add(lblModifyCurrentValue);
		  
		  JButton btnSave = new JButton("Save!");
		  btnSave.addActionListener(new ActionListener() {
		  	public void actionPerformed(ActionEvent e) {
		  		frame.dispose();
		  	}
		  });
		  btnSave.setBounds(201, 106, 89, 23);
		  frame.getContentPane().add(btnSave);

		  comboBox.setBounds(10, 36, 280, 23);
		  frame.getContentPane().add(comboBox);
		  
		  JLabel lblModifyCurrentSemestre = new JLabel("Modify current Semester value");
		  lblModifyCurrentSemestre.setBounds(10, 70, 280, 14);
		  frame.getContentPane().add(lblModifyCurrentSemestre);
		  spinner.setBounds(10, 95, 30, 20);
		  frame.getContentPane().add(spinner);
		  
		  frame.setVisible(true);
	}
}
