package gui.SeparateWindows;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.tree.TreeModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import data.gry.Tag;
import data.informacje.DaneOsobowe;
import data.informacje.Indeks;
import data.studia.Lokalizacja;
import tree.implementation.gamenodes.ListaTagowNode;
import tree.implementation.gamenodes.TagNode;
import tree.implementation.gamenodes.GraNode;
import tree.implementation.informacjenodes.AutorNode;
import tree.implementation.informacjenodes.DaneOsoboweNode;
import tree.implementation.informacjenodes.IndeksNode;
import tree.implementation.informacjenodes.ListaIndeksowNode;
import tree.implementation.studianodes.LokalizacjaNode;
import tree.implementation.studianodes.StudioNode;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

public class LokacjaModifyObject {
	private static JTextField textField;
	private static String s;
	private static JTextField txtKraj;
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void ModifyWindow(LokalizacjaNode locNode, TreeModel tm) {
		  JFrame frame = new JFrame("Modify Existing Object");
		  frame.addWindowListener(new java.awt.event.WindowAdapter() {
			    @Override
			    public void windowClosed(java.awt.event.WindowEvent windowEvent) {
			    	StudioNode aut = (StudioNode) locNode.getParent();
			    	((DefaultTreeModel) tm).removeNodeFromParent(locNode);
			    	Lokalizacja loco = new Lokalizacja();
			    	loco.setStan(textField.getText());
			    	loco.setKraj(txtKraj.getText());
			    	aut.getStudio().setLokalizacja(loco);
			    	((DefaultTreeModel) tm).insertNodeInto(new LokalizacjaNode(loco),aut,aut.getChildCount() );
			    	}
			});
		  frame.setSize( 300,175 );
		  frame.setLocationRelativeTo( null );
		  frame.getContentPane().setLayout(null);
		  frame.setUndecorated(true);
		  frame.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		  textField = new JTextField();
		  textField.setBounds(10, 40, 244, 23);
		  textField.setText(locNode.getLokalizacja().getStan());
		  frame.getContentPane().add(textField);
		  textField.setColumns(10);
		  
		  JLabel lblModifyCurrentValue = new JLabel("Modify Current Stan");
		  lblModifyCurrentValue.setBounds(10, 15, 151, 14);
		  frame.getContentPane().add(lblModifyCurrentValue);
		  
		  JButton btnSave = new JButton("Save!");
		  btnSave.addActionListener(new ActionListener() {
		  	public void actionPerformed(ActionEvent e) {
		  		if(txtKraj.getText().length()>=1) {
		  			frame.dispose();
		  		}
		  		else {
		  			JOptionPane.showMessageDialog(frame,
						    "No input detected.",
						    "Input incorrect",
						    JOptionPane.ERROR_MESSAGE);
		  		}
		  	}
		  });
		  btnSave.setBounds(201, 134, 89, 23);
		  frame.getContentPane().add(btnSave);
		  
		  JLabel lblModifyCurrentNazwisko = new JLabel("Modify Current Kraj");
		  lblModifyCurrentNazwisko.setBounds(10, 75, 151, 14);
		  frame.getContentPane().add(lblModifyCurrentNazwisko);
		  
		  txtKraj = new JTextField();
		  txtKraj.setText(locNode.getLokalizacja().getKraj());
		  txtKraj.setBounds(10, 100, 244, 23);
		  frame.getContentPane().add(txtKraj);
		  txtKraj.setColumns(10);
		  
		 
		  
		  frame.setVisible(true);
	}
}
