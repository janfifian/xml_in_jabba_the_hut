package gui.SeparateWindows;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.tree.TreeModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import data.gry.Tag;
import data.informacje.Indeks;
import tree.implementation.gamenodes.ListaTagowNode;
import tree.implementation.gamenodes.TagNode;
import tree.implementation.gamenodes.GraNode;
import tree.implementation.informacjenodes.IndeksNode;
import tree.implementation.informacjenodes.ListaIndeksowNode;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class TagModifyObject {
	private static JTextField textField;
	private static String s;
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void ModifyWindow(TagNode tagNode, TreeModel tm) {
		  JFrame frame = new JFrame("Modify Existing Object");
		  frame.addWindowListener(new java.awt.event.WindowAdapter() {
			    @Override
			    public void windowClosed(java.awt.event.WindowEvent windowEvent) {
			    	ListaTagowNode lt = (ListaTagowNode) tagNode.getParent();
			    	((DefaultTreeModel) tm).removeNodeFromParent(tagNode);
			    	((GraNode) lt.getParent()).getGra().getListaTagow().getTagi().remove(tagNode.getTag());
			    	Tag ind = new Tag();
			    	ind.setTag(s);
			    	((GraNode) lt.getParent()).getGra().getListaTagow().getTagi().add(ind);
			    	((DefaultTreeModel) tm).insertNodeInto(new TagNode(ind),lt,lt.getChildCount() );
			    }
			});
		  frame.setSize( 300,140 );
		  frame.setLocationRelativeTo( null );
		  frame.getContentPane().setLayout(null);
		  frame.setUndecorated(true);
		  frame.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		  textField = new JTextField();
		  textField.setBounds(10, 40, 244, 40);
		  textField.setText(tagNode.getTag().getTag());
		  frame.getContentPane().add(textField);
		  textField.setColumns(10);
		  
		  JLabel lblModifyCurrentValue = new JLabel("Modify current Tag value");
		  lblModifyCurrentValue.setBounds(10, 11, 151, 14);
		  frame.getContentPane().add(lblModifyCurrentValue);
		  
		  JButton btnSave = new JButton("Save!");
		  btnSave.addActionListener(new ActionListener() {
		  	public void actionPerformed(ActionEvent e) {
		  		 s = textField.getText();
		  		if(s.length()>=1) {
		  		frame.dispose();
		  		}
		  		else {
		  			JOptionPane.showMessageDialog(frame,
						    "No input detected.",
						    "Input incorrect",
						    JOptionPane.ERROR_MESSAGE);
		  		}
		  	}
		  });
		  btnSave.setBounds(185, 91, 89, 23);
		  frame.getContentPane().add(btnSave);
		  
		  frame.setVisible(true);
	}
}
