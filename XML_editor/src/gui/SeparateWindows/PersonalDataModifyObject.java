package gui.SeparateWindows;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.tree.TreeModel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import data.gry.Tag;
import data.informacje.DaneOsobowe;
import data.informacje.Indeks;
import tree.implementation.gamenodes.ListaTagowNode;
import tree.implementation.gamenodes.TagNode;
import tree.implementation.gamenodes.GraNode;
import tree.implementation.informacjenodes.AutorNode;
import tree.implementation.informacjenodes.DaneOsoboweNode;
import tree.implementation.informacjenodes.IndeksNode;
import tree.implementation.informacjenodes.ListaIndeksowNode;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

public class PersonalDataModifyObject {
	private static JTextField textField;
	private static String s;
	private static JTextField txtNazwisko;
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void ModifyWindow(DaneOsoboweNode persDataNode, TreeModel tm) {
		  JFrame frame = new JFrame("Modify Existing Object");
		  String[] possibleTytuly = {"","mgr","mgr in�.", "in�.", "dr","dr in�.", "dr hab.", "dr hab. in�.", "prof. dr hab.", "prof. dr hab. in�."};
		  JComboBox comboBox = new JComboBox(possibleTytuly);
		  comboBox.setBounds(10, 169, 244, 22);
		  frame.getContentPane().add(comboBox);
		  frame.addWindowListener(new java.awt.event.WindowAdapter() {
			    @Override
			    public void windowClosed(java.awt.event.WindowEvent windowEvent) {
			    	AutorNode aut = (AutorNode) persDataNode.getParent();
			    	((DefaultTreeModel) tm).removeNodeFromParent(persDataNode);
			    	DaneOsobowe persData = new DaneOsobowe();
			    	persData.setImie(textField.getText());
			    	persData.setNazwisko(txtNazwisko.getText());
			    	persData.setTytul_naukowy( (String) comboBox.getSelectedItem() );
			    	aut.getAutor().setDaneOsobowe(persData);
			    	((DefaultTreeModel) tm).insertNodeInto(new DaneOsoboweNode(persData),aut,aut.getChildCount() );
			    	}
			});
		  frame.setSize( 300,245 );
		  frame.setLocationRelativeTo( null );
		  frame.getContentPane().setLayout(null);
		  frame.setUndecorated(true);
		  frame.getRootPane().setWindowDecorationStyle(JRootPane.NONE);
		  textField = new JTextField();
		  textField.setBounds(10, 40, 244, 23);
		  textField.setText(persDataNode.getDaneOsobowe().getImie());
		  frame.getContentPane().add(textField);
		  textField.setColumns(10);
		  
		  JLabel lblModifyCurrentValue = new JLabel("Modify Current Imi\u0119");
		  lblModifyCurrentValue.setBounds(10, 15, 151, 14);
		  frame.getContentPane().add(lblModifyCurrentValue);
		  
		  JButton btnSave = new JButton("Save!");
		  btnSave.addActionListener(new ActionListener() {
		  	public void actionPerformed(ActionEvent e) {
		  		if(textField.getText().length()>=1 && txtNazwisko.getText().length()>=1) {
		  			frame.dispose();
		  		}
		  		else {
		  			JOptionPane.showMessageDialog(frame,
						    "No input detected.",
						    "Input incorrect",
						    JOptionPane.ERROR_MESSAGE);
		  		}
		  	}
		  });
		  btnSave.setBounds(201, 202, 89, 23);
		  frame.getContentPane().add(btnSave);
		  
		  JLabel lblModifyCurrentNazwisko = new JLabel("Modify Current Nazwisko");
		  lblModifyCurrentNazwisko.setBounds(10, 75, 151, 14);
		  frame.getContentPane().add(lblModifyCurrentNazwisko);
		  
		  txtNazwisko = new JTextField();
		  txtNazwisko.setText(persDataNode.getDaneOsobowe().getNazwisko());
		  txtNazwisko.setBounds(10, 100, 244, 23);
		  frame.getContentPane().add(txtNazwisko);
		  txtNazwisko.setColumns(10);
		  
		  JLabel lblSelectBefittingTytu = new JLabel("Select Befitting Tytu\u0142 Naukowy");
		  lblSelectBefittingTytu.setBounds(11, 144, 243, 14);
		  frame.getContentPane().add(lblSelectBefittingTytu);
		  
		 
		  
		  frame.setVisible(true);
	}
}
