package gui.SeparateWindows;

import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;

import javax.swing.tree.DefaultTreeModel;

import data.Groteka;
import data.gry.Gra;
import data.gry.ListaTagow;
import data.gry.Tag;
import data.informacje.Autor;
import data.informacje.DaneOsobowe;
import data.informacje.Indeks;
import data.informacje.ListaIndeksow;
import data.studia.Lokalizacja;
import data.studia.Studio;
import tree.implementation.gamenodes.GraNode;
import tree.implementation.gamenodes.ListaTagowNode;
import tree.implementation.gamenodes.TagNode;
import tree.implementation.informacjenodes.AutorNode;
import tree.implementation.informacjenodes.DaneOsoboweNode;
import tree.implementation.informacjenodes.IndeksNode;
import tree.implementation.informacjenodes.ListaIndeksowNode;
import tree.implementation.studianodes.LokalizacjaNode;
import tree.implementation.studianodes.StudioNode;

import java.awt.BorderLayout;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSpinner;

public class GameModifierWindow {

	JFrame frame;
	private static JTextField txtNazwastudia;
	private static JTextField txtStan;
	private static JTextField txtKraj;
	private static JTextField txtOpisStudia;
	private static JTextField textData;
	private static JTextField idField;
	private static JTextField txtGamename;
	private static JTextField textField;
	private static JTextField txtCopiesSold;
	private static final JLabel lblDataWydania = new JLabel("Data Wydania:");
	private static JTextField txtDataWydania;
	private static JTextField txtOpis;
	private static JTextField txtIndexField;
	private static JTextField txtAnonimowy;
	private static JTextField txtAnon;
	/**
	 * @wbp.parser.entryPoint
	 */
	public static void GameWindow(TreeModel treeModel, Groteka groteka) {
	    JFrame frame = new JFrame("Add New Object Window" );
	    frame.setSize( 500,200 );
	    frame.setLocationRelativeTo( null );
	    frame.getContentPane().setLayout(null);
	    
	    JPanel AuthorPanel = new JPanel();
	    AuthorPanel.setBounds(0, 24, 484, 137);
	    frame.getContentPane().add(AuthorPanel);
	    AuthorPanel.setLayout(null);
	    /*
	     * This Part is for Studio Adding:
	     * 
	     */
	    JPanel StudioPanel = new JPanel();
	    StudioPanel.setOpaque(false);
	    StudioPanel.setBounds(10, 33, 464, 122);
	    frame.getContentPane().add(StudioPanel);
	    StudioPanel.setLayout(null);
	    JLabel lblNazwa = new JLabel("Nazwa:");
	    lblNazwa.setBounds(0, 0, 96, 20);
	    StudioPanel.add(lblNazwa);
	    
	    txtNazwastudia = new JTextField();
	    txtNazwastudia.setBounds(90, 0, 200, 20);
	    StudioPanel.add(txtNazwastudia);
	    txtNazwastudia.setText("NazwaStudia");
	    txtNazwastudia.setColumns(10);
	    
	    txtStan = new JTextField();
	    txtStan.setBounds(90, 31, 96, 20);
	    StudioPanel.add(txtStan);
	    txtStan.setText("Stan");
	    txtStan.setColumns(10);
	    
	    txtKraj = new JTextField();
	    txtKraj.setBounds(290, 31, 96, 20);
	    StudioPanel.add(txtKraj);
	    txtKraj.setText("Kraj");
	    txtKraj.setColumns(10);
	    
	    JLabel lblStan = new JLabel("Stan:");
	    lblStan.setBounds(0, 31, 96, 20);
	    StudioPanel.add(lblStan);
	    
	    JLabel lblKraj = new JLabel("Kraj:");
	    lblKraj.setBounds(196, 31, 96, 22);
	    StudioPanel.add(lblKraj);
	    
	    JLabel lblOpis = new JLabel("Opis:");
	    lblOpis.setBounds(0, 62, 96, 20);
	    StudioPanel.add(lblOpis);
	    
	    txtOpisStudia = new JTextField();
	    txtOpisStudia.setBounds(90, 62, 200, 20);
	    StudioPanel.add(txtOpisStudia);
	    txtOpisStudia.setText("Opis Studia");
	    txtOpisStudia.setColumns(10);
	    
	    JLabel lblDataZaoenia = new JLabel("Data Za\u0142o\u017Cenia:");
	    lblDataZaoenia.setBounds(0, 103, 96, 14);
	    StudioPanel.add(lblDataZaoenia);
	    
	    textData = new JTextField();
	    textData.setBounds(90, 100, 96, 20);
	    StudioPanel.add(textData);
	    textData.setColumns(10);
	    
	    	    JButton btnDodaj = new JButton("Dodaj!");
	    	    btnDodaj.setBounds(375, 99, 89, 23);
	    	    StudioPanel.add(btnDodaj);
	    	    
	    	    JLabel lblDdmmyyyy = new JLabel("YYYY-MM-DD");
	    	    lblDdmmyyyy.setBounds(196, 103, 94, 14);
	    	    StudioPanel.add(lblDdmmyyyy);
	    	    
	    	    JLabel lblId = new JLabel("Id:");
	    	    lblId.setBounds(300, 3, 48, 14);
	    	    StudioPanel.add(lblId);
	    	    
	    	    idField = new JTextField();
	    	    idField.setBounds(332, 0, 120, 20);
	    	    StudioPanel.add(idField);
	    	    idField.setColumns(10);
	    	    btnDodaj.addActionListener(new ActionListener() {
	    	    	public void actionPerformed(ActionEvent e) {
	    	    	Studio st = new Studio();
		    if(textData.getText().matches("(19[0-9][0-9]|20[0-1][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|30|31)") && groteka.getLista_studiow().checkIdUniqueness(idField.getText()) ){
		    	st.setData_zalozenia(textData.getText());
		    	st.setId(idField.getText());
		    	st.setNazwa(txtNazwastudia.getText());
		    	st.setOpis(txtOpisStudia.getText());
		    	Lokalizacja lk = new Lokalizacja();
		    	lk.setKraj(txtKraj.getText());
		    	lk.setStan(txtStan.getText());
		    	st.setLokalizacja(lk);
		    	groteka.getLista_studiow().getStudia().add(st);
				StudioNode node = new StudioNode(st);
				LokalizacjaNode lknode = new LokalizacjaNode(lk);
				node.add(lknode);
				((DefaultTreeModel) treeModel).insertNodeInto(node,(MutableTreeNode) ((MutableTreeNode) treeModel.getRoot()).getChildAt(0).getChildAt(0), ((MutableTreeNode) treeModel.getRoot()).getChildAt(0).getChildAt(0).getChildCount() );
		    	System.out.println("Studio added to the Groteka");
			}
			else
			{
				JOptionPane.showMessageDialog(frame,
					    "Either data format is not correct or id is not unique.",
					    "Input incorrect",
					    JOptionPane.ERROR_MESSAGE);
			}
	    	    	
	    	    	
	    	    	}
	    	    });
	    	    
	    	    
	    	    /*
	    	     * This Part is for Gra Adding:
	    	     * 
	    	     */
	    JPanel GamePanel = new JPanel();
	    GamePanel.setBounds(5, 33, 469, 128);
	    frame.getContentPane().add(GamePanel);
	    GamePanel.setLayout(null);
	    
	    Studio[] studioValues = new Studio[groteka.getLista_studiow().getStudia().size()];
		String[] studioPossibleValues = new String[groteka.getLista_studiow().getStudia().size()];
	    for(int i = 0; i < groteka.getLista_studiow().getStudia().size(); i++) {
	    	studioValues[i] = groteka.getLista_studiow().getStudia().get(i);
	    	studioPossibleValues[i] =  groteka.getLista_studiow().getStudia().get(i).getId();
	    }
	    JComboBox studioSelector = new JComboBox(studioPossibleValues);
	    studioSelector.setBounds(90, 11, 174, 22);
	    GamePanel.add(studioSelector);
	    
	    JLabel lblSelectStudio = new JLabel("Studio:");
	    lblSelectStudio.setBounds(10, 15, 117, 14);
	    GamePanel.add(lblSelectStudio);
	    
	    JLabel lblNazwa_1 = new JLabel("Nazwa:");
	    lblNazwa_1.setBounds(274, 15, 48, 14);
	    GamePanel.add(lblNazwa_1);
	    
	    txtGamename = new JTextField();
	    txtGamename.setText("GameName");
	    txtGamename.setBounds(321, 12, 146, 20);
	    GamePanel.add(txtGamename);
	    txtGamename.setColumns(10);
	    
	    JLabel lblCenaWUsd = new JLabel("Cena w USD:");
	    lblCenaWUsd.setBounds(10, 40, 78, 14);
	    GamePanel.add(lblCenaWUsd);
	    
	    SpinnerModel ocenamodel = new SpinnerNumberModel(3, 1, 5, 1); 
	    JSpinner ocenaSpinner_1 = new JSpinner(ocenamodel);
	    ocenaSpinner_1.setBounds(410, 43, 50, 22);
	    GamePanel.add(ocenaSpinner_1);
	    
	    JLabel lblOcena = new JLabel("Ocena:");
	    lblOcena.setBounds(352, 47, 48, 14);
	    GamePanel.add(lblOcena);
	    
	    textField = new JTextField();
	    textField.setText("0.00");
	    textField.setBounds(90, 37, 96, 20);
	    GamePanel.add(textField);
	    textField.setColumns(10);
	    
	    JLabel lblLiczbaTagw = new JLabel("Liczba Tag\u00F3w:");
	    lblLiczbaTagw.setBounds(204, 43, 105, 14);
	    GamePanel.add(lblLiczbaTagw);
	    
	    SpinnerModel tagmodel = new SpinnerNumberModel(0, 0, 6, 1);
	    JSpinner spinner = new JSpinner(tagmodel);
	    spinner.setBounds(312, 40, 30, 20);
	    GamePanel.add(spinner);
	    
	    JLabel lblLiczbaSprzedanychKopii = new JLabel("Liczba Sprzedanych Kopii:");
	    lblLiczbaSprzedanychKopii.setBounds(10, 65, 193, 14);
	    GamePanel.add(lblLiczbaSprzedanychKopii);
	    
	    txtCopiesSold = new JTextField();
	    txtCopiesSold.setText("200000");
	    txtCopiesSold.setBounds(183, 62, 96, 20);
	    GamePanel.add(txtCopiesSold);
	    txtCopiesSold.setColumns(10);
	    lblDataWydania.setBounds(11, 85, 98, 22);
	    GamePanel.add(lblDataWydania);
	    
	    txtDataWydania = new JTextField();
	    txtDataWydania.setText("01-01-2000");
	    txtDataWydania.setBounds(99, 86, 96, 20);
	    GamePanel.add(txtDataWydania);
	    txtDataWydania.setColumns(10);
	    
	    txtOpis = new JTextField();
	    txtOpis.setText("Opis...");
	    txtOpis.setBounds(255, 85, 96, 20);
	    GamePanel.add(txtOpis);
	    txtOpis.setColumns(10);
	    
	    JLabel lblOpis_1 = new JLabel("Opis:");
	    lblOpis_1.setBounds(204, 88, 48, 14);
	    GamePanel.add(lblOpis_1);
	    //Gra
	    JButton btnDodaj_1 = new JButton("Dodaj!");
	    btnDodaj_1.addActionListener(new ActionListener() {
	    	Gra gra = new Gra();
	    	public void actionPerformed(ActionEvent e) {
	    		try {
	    			gra.setCenaWUSD(Float.parseFloat(textField.getText()));
	    			}
	    		catch(NumberFormatException exc) {
	    			JOptionPane.showMessageDialog(frame,
						    "Improper Cena w USD format!!",
						    "Input incorrect",
						    JOptionPane.ERROR_MESSAGE);
	    		return;
	    		}
	    		catch(Exception exc)
	    			{
	    				exc.printStackTrace();
	    				return;
	    			}
	    		
	    		try {
	    				gra.setLiczbaSprzedanychKopii(Integer.parseInt(txtCopiesSold.getText()));
	    			}
	    		catch(NumberFormatException exc) {
	    			JOptionPane.showMessageDialog(frame,
						    "Improper Liczba Sprzedanych Kopii format!!",
						    "Input incorrect",
						    JOptionPane.ERROR_MESSAGE);
	    		return;
	    		}
	    		catch(Exception exc)
	    			{
	    				exc.printStackTrace();
	    				return;
	    			}
	    		if(txtDataWydania.getText().matches("(19[0-9][0-9]|20[0-1][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|30|31)")) {
	    			gra.setDataWydania(txtDataWydania.getText());
	    			gra.setOcena((int) ocenaSpinner_1.getValue());
	    			gra.setOpisGry(txtOpis.getText());
	    			int studindex = studioSelector.getSelectedIndex();
	    			if(studindex<0) {
	    				JOptionPane.showMessageDialog(frame,
							    "Improper Studio selection!",
							    "Input incorrect",
							    JOptionPane.ERROR_MESSAGE);
	    				return;
	    			}
	    			else {
	    				gra.setStudio(studioValues[studindex]);
	    			}
	    			if(txtGamename.getText().length()<1) {
	    				JOptionPane.showMessageDialog(frame,
							    "No Nazwa of the Gra has been entered!",
							    "Input incorrect",
							    JOptionPane.ERROR_MESSAGE);
	    				return;
	    			}
	    			else {
	    				gra.setNazwa(txtGamename.getText());
	    			}
	    			ListaTagow taglist = new ListaTagow();
	    			taglist.setTagi(new ArrayList<Tag>());
	    			ListaTagowNode taglistNode = new ListaTagowNode(taglist);
	    			for(int i = 0; i < (int)spinner.getValue(); i++) {
	    			Tag tage = new Tag();
	    			tage.setTag("randomTag"+Integer.toString(i));
	    			TagNode tageNode = new TagNode(tage);
	    			taglistNode.add(tageNode);
	    			}
	    			GraNode gameNode = new GraNode(gra);
	    			gameNode.add(taglistNode);
	    			groteka.getBiblioteka_gier().getGry().add(gra);
	    			((DefaultTreeModel) treeModel).insertNodeInto(gameNode,(MutableTreeNode) ((MutableTreeNode) treeModel.getRoot()).getChildAt(0).getChildAt(1), ((MutableTreeNode) treeModel.getRoot()).getChildAt(0).getChildAt(1).getChildCount() );
	    		}
	    		else {
	    			JOptionPane.showMessageDialog(frame,
						    "Improper Data Wydania format!!",
						    "Input incorrect",
						    JOptionPane.ERROR_MESSAGE);
	    			}
	    		}
	    	}
	    );
	    btnDodaj_1.setBounds(371, 85, 89, 23);
	    GamePanel.add(btnDodaj_1);
	    GamePanel.setVisible(false);
	    
	    String[] possibleChoices = {"Studio", "Gra", "Autor"};
	    JComboBox entryAddBox = new JComboBox(possibleChoices);
	    entryAddBox.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		if(((String) entryAddBox.getSelectedItem()).contentEquals("Studio")) {
	    			StudioPanel.setVisible(true);
	    		}
	    		else {
	    			StudioPanel.setVisible(false);
	    		}
	    		if(((String) entryAddBox.getSelectedItem()).contentEquals("Gra")) {
	    			GamePanel.setVisible(true);
	    		}
	    		else {
	    			GamePanel.setVisible(false);
	    		}
	    		if(((String) entryAddBox.getSelectedItem()).contentEquals("Autor")) {
	    			AuthorPanel.setVisible(true);
	    		}
	    		else {
	    			AuthorPanel.setVisible(false);
	    		}
	    	}
	    });
	    entryAddBox.setBounds(0, 0, 143, 22);
	    frame.getContentPane().add(entryAddBox);



	    /*
	     * This Part is for Autor Adding:
	     * 
	     */
	    JLabel lblImi = new JLabel("Imi\u0119:");
	    lblImi.setBounds(20, 9, 48, 14);
	    AuthorPanel.add(lblImi);
	    
	    JLabel lblNazwisko = new JLabel("Nazwisko:");
	    lblNazwisko.setBounds(20, 34, 79, 14);
	    AuthorPanel.add(lblNazwisko);
	    
	    JLabel lblTytu = new JLabel("Tytu\u0142:");
	    lblTytu.setBounds(20, 59, 48, 14);
	    AuthorPanel.add(lblTytu);
	    
	    JLabel lblIndeks = new JLabel("Indeks:");
	    lblIndeks.setBounds(20, 84, 48, 14);
	    AuthorPanel.add(lblIndeks);
	    
	    JLabel lblRodzajRejestracji = new JLabel("Rodzaj rejestracji:");
	    lblRodzajRejestracji.setBounds(237, 12, 141, 14);
	    AuthorPanel.add(lblRodzajRejestracji);
	    
	    String[] rejestracjePossible = {"Pe�na", "Warunkowa", "Dzieka�ska"};
	    JComboBox rejestracjaComboBox = new JComboBox(rejestracjePossible);
	    rejestracjaComboBox.setBounds(237, 30, 222, 22);
	    AuthorPanel.add(rejestracjaComboBox);
	    
	    JLabel lblSemestr = new JLabel("Semestr:");
	    lblSemestr.setBounds(237, 59, 94, 14);
	    AuthorPanel.add(lblSemestr);
	    
	    SpinnerModel smstrmodel = new SpinnerNumberModel(6, 1, 10, 1); 
	    JSpinner semestrSpinner_1 = new JSpinner(smstrmodel);
	    semestrSpinner_1.setBounds(341, 56, 30, 20);
	    AuthorPanel.add(semestrSpinner_1);
	    
	    String[] possibleTytuly = {"","mgr","mgr in�.", "in�.", "dr","dr in�.", "dr hab.", "dr hab. in�.", "prof. dr hab.", "prof. dr hab. in�."};
	    JComboBox tytulComboBox = new JComboBox(possibleTytuly);
	    tytulComboBox.setBounds(55, 55, 173, 21);
	    AuthorPanel.add(tytulComboBox);
	    //dodaje autora
	    JButton btnDodaj_2 = new JButton("Dodaj!");
	    btnDodaj_2.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		if(txtAnon.getText().length()<1 && txtAnonimowy.getText().length()<1) {
	    			JOptionPane.showMessageDialog(frame,
						    "Name and/or surname shouldn't be THAT short.",
						    "Input incorrect",
						    JOptionPane.ERROR_MESSAGE);
	    		}
	    		else {
	    			if(txtIndexField.getText().matches("[0-9][0-9][0-9][0-9][0-9][0-9]")) {
	    				Autor aut = new Autor();
	    				ListaIndeksow indlist = new ListaIndeksow();
	    				indlist.setLista_indeksow(new ArrayList<Indeks>());
	    				Indeks ind = new Indeks();
	    				ind.setIndex(txtIndexField.getText());
	    				ind.setAktualny("Tak");
	    				indlist.getLista_indeksow().add(ind);
	    				aut.setListaIndeksow(indlist);
	    				ListaIndeksowNode indlistNode = new ListaIndeksowNode(indlist);
	    				indlistNode.add(new IndeksNode(ind));
	    				DaneOsobowe persData = new DaneOsobowe();
	    				persData.setImie(txtAnon.getText());
	    				persData.setNazwisko(txtAnonimowy.getText());
	    				persData.setTytul_naukowy((String) tytulComboBox.getSelectedItem());
	    				aut.setDaneOsobowe(persData);
	    				DaneOsoboweNode persDataNode = new DaneOsoboweNode(persData);
	    				aut.setRejestracja((String) rejestracjaComboBox.getSelectedItem());
	    				aut.setSemestr((int)semestrSpinner_1.getValue());
	    				AutorNode node = new AutorNode(aut);
	    				node.add(indlistNode);
	    				node.add(persDataNode);
	    				groteka.getInformacje().getLista_autorow().add(aut);
	    				((DefaultTreeModel) treeModel).insertNodeInto(node,(MutableTreeNode) ((MutableTreeNode) treeModel.getRoot()).getChildAt(0).getChildAt(2), ((MutableTreeNode) treeModel.getRoot()).getChildAt(0).getChildAt(2).getChildCount() );
	    		    	System.out.println("Autor added to the Groteka");
	    			}
	    			else {
	    				JOptionPane.showMessageDialog(frame,
							    "Improper index format.",
							    "Input incorrect",
							    JOptionPane.ERROR_MESSAGE);
	    			}
	    		}
	     	}
	    });
	    btnDodaj_2.setBounds(362, 92, 89, 23);
	    AuthorPanel.add(btnDodaj_2);
	    
	    txtIndexField = new JTextField();
	    txtIndexField.setText("000000");
	    txtIndexField.setBounds(65, 81, 96, 20);
	    AuthorPanel.add(txtIndexField);
	    txtIndexField.setColumns(10);
	    
	    txtAnonimowy = new JTextField();
	    txtAnonimowy.setText("Anonimowy");
	    txtAnonimowy.setBounds(95, 31, 134, 17);
	    AuthorPanel.add(txtAnonimowy);
	    txtAnonimowy.setColumns(10);
	    
	    txtAnon = new JTextField();
	    txtAnon.setText("Anon");
	    txtAnon.setBounds(55, 9, 173, 17);
	    AuthorPanel.add(txtAnon);
	    txtAnon.setColumns(10);
	    AuthorPanel.setVisible(false);
	    frame.setVisible( true );  
	}
}
