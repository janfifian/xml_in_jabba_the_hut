package gui;

import tree.implementation.studianodes.*;
import tree.implementation.informacjenodes.*;
import tree.implementation.gamenodes.*;

import javax.swing.table.TableModel;
import javax.swing.table.DefaultTableModel;

public class TableViewerHandler {
		public static void HandleNode(Object TreeNode, TableModel tab) {
			if(TreeNode.getClass().getName() == "tree.implementation.GrotekaNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Groteka","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.studianodes.ListaStudiowNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Lista Studi�w","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.gamenodes.BibliotekaGierNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Biblioteka Gier","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.informacjenodes.InformacjeNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Informacje","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.studianodes.StudioNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Studio","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				String[] idRowData = {"id","Attribute",((StudioNode) TreeNode).getStudio().getId()};
				((DefaultTableModel) tab).addRow(idRowData);
				String[] nazwaRowData = {"Nazwa","Element",((StudioNode) TreeNode).getStudio().getNazwa()};
				((DefaultTableModel) tab).addRow(nazwaRowData);
				String[] lokalizacjaRowData = {"Lokalizacja","Complex Element",""};
				((DefaultTableModel) tab).addRow(lokalizacjaRowData);
				String[] opisRowData = {"Opis Studia","Element",((StudioNode) TreeNode).getStudio().getOpis()};
				((DefaultTableModel) tab).addRow(opisRowData);
				String[] dataZalozeniaRowData = {"Data za�o�enia","Element",((StudioNode) TreeNode).getStudio().getData_zalozenia()};
				((DefaultTableModel) tab).addRow(dataZalozeniaRowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.studianodes.LokalizacjaNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Lokalizacja","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				String[] stanRowData = {"Stan","Element",((LokalizacjaNode) TreeNode).getLokalizacja().getStan()};
				((DefaultTableModel) tab).addRow(stanRowData);
				String[] krajRowData = {"Kraj","Element",((LokalizacjaNode) TreeNode).getLokalizacja().getKraj()};
				((DefaultTableModel) tab).addRow(krajRowData);
				return;
				}
			if(TreeNode.getClass().getName() == "tree.implementation.informacjenodes.AutorNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Autor","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				String[] daneOsoboweRowData = {"Dane Osobowe","Complex Element",""};
				((DefaultTableModel) tab).addRow(daneOsoboweRowData);
				String[] indexlistRowData = {"Lista Indeks�w","Complex Element",""};
				((DefaultTableModel) tab).addRow(indexlistRowData);
				String[] rejestracjaRowData = {"Rejestracja","Element",((AutorNode) TreeNode).getAutor().getRejestracja()};
				((DefaultTableModel) tab).addRow(rejestracjaRowData);
				String[] semestrRowData = {"Semestr","Element",Integer.toString(((AutorNode) TreeNode).getAutor().getSemestr())};
				((DefaultTableModel) tab).addRow(semestrRowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.informacjenodes.DaneOsoboweNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"DaneOsobowe","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				String[] imieRowData = {"Imi�","Element",((DaneOsoboweNode) TreeNode).getDaneOsobowe().getImie()};
				((DefaultTableModel) tab).addRow(imieRowData);
				String[] nazwiskoRowData = {"Nazwisko","Element",((DaneOsoboweNode) TreeNode).getDaneOsobowe().getNazwisko()};
				((DefaultTableModel) tab).addRow(nazwiskoRowData);
				String[] tytulNaukowyRowData = {"Tytu� naukowy","Element",((DaneOsoboweNode) TreeNode).getDaneOsobowe().getTytul_naukowy()};
				((DefaultTableModel) tab).addRow(tytulNaukowyRowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.informacjenodes.ListaIndeksowNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Lista Indeks�w","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.informacjenodes.IndeksNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Indeks","Element",((IndeksNode) TreeNode).getIndex().getIndex()};
				((DefaultTableModel) tab).addRow(rowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.gamenodes.BibliotekaGierNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Biblioteka Gier","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.gamenodes.ListaTagowNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Lista Tag�w","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.gamenodes.TagNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Tag","Element",((TagNode) TreeNode).getTag().getTag()};
				((DefaultTableModel) tab).addRow(rowData);
				return;
			}
			if(TreeNode.getClass().getName() == "tree.implementation.gamenodes.GraNode") {
				((DefaultTableModel) tab).setRowCount(0);
				String[] rowData = {"Gra","Complex Element",""};
				((DefaultTableModel) tab).addRow(rowData);
				String[] nazwaRowData = {"Nazwa", "Element",((GraNode) TreeNode).getGra().getNazwa()};
				((DefaultTableModel) tab).addRow(nazwaRowData);
				String[] ocenaRowData = {"Nazwa", "Element",Integer.toString(((GraNode) TreeNode).getGra().getOcena())};
				((DefaultTableModel) tab).addRow(ocenaRowData);
				String[] taglistRowData = {"Lista Tag�w","Complex Element",""};
				((DefaultTableModel) tab).addRow(taglistRowData);
				String[] opisGryRowData = {"Opis Gry", "Element",((GraNode) TreeNode).getGra().getOpisGry()} ;
				((DefaultTableModel) tab).addRow(opisGryRowData);
				String[] cenaWUSDRowData= {"Cena w USD", "Element",Float.toString(((GraNode) TreeNode).getGra().getCenaWUSD())} ;
				((DefaultTableModel) tab).addRow(cenaWUSDRowData);
				String[] liczbaSprzedanychKopiiRowData= {"Liczba Sprzedanych Kopii", "Element",Integer.toString(((GraNode) TreeNode).getGra().getLiczbaSprzedanychKopii())} ;;
				((DefaultTableModel) tab).addRow(liczbaSprzedanychKopiiRowData);
				String[] dataWydaniaRowData= {"Data Wydania", "Element",((GraNode) TreeNode).getGra().getDataWydania()} ;;
				((DefaultTableModel) tab).addRow(dataWydaniaRowData);
				String[] studioRowData= {"Studio", "Element",((GraNode) TreeNode).getGra().getStudio().getNazwa()} ;;
				((DefaultTableModel) tab).addRow(studioRowData);
				return;
			}
		}
}
