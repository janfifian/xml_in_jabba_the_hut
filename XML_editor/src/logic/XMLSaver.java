package logic;

import java.io.File;
import java.text.Format;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import data.Groteka;

public class XMLSaver {

	public void saveToXML(String filename) {
		System.out.println("Saved as "+filename);
	}

	public void saveToXML(Groteka groteka, File selectedFile) {
		try
		{
			Serializer ser = new Persister();
			
			ser.write(groteka, selectedFile);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("Saved as "+selectedFile);
	}
}
