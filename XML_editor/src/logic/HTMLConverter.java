package logic;

import java.io.File;

public class HTMLConverter {
	public void convertToHTML(String filename) {
		System.out.println("Output file located at: "+filename);
	}

	public void convertToHTML(File XMLSourceFile, File XSLTSourceFile, File OutputFile) {
		XSLTTransformer.xsl(XMLSourceFile.getAbsolutePath(), OutputFile.getAbsolutePath(), XSLTSourceFile.getAbsolutePath());
		System.out.println("Output file located at: "+OutputFile);
	}
}
