﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:gtk="https://www.example.com"
	exclude-result-prefixes="gtk">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/groteka">
		<html>
			<head>
				<title>Groteka</title>
				<meta charset="UTF-8"/>
				<meta name="description" content="Lista gier komputerowych i studiów"/>
				<meta name="keywords" content="gra komputerowa, studio komputerowe"/>
				<xsl:for-each select="informacje/autor">
					<meta name="author" content="dane_autora">
						<xsl:attribute name="content">
							<xsl:value-of select="imię"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="nazwisko"/>
						</xsl:attribute>
					</meta>
				</xsl:for-each>
				<link rel="stylesheet" type="text/css" href="Zad5_Jan_Filipowicz_Filip_Turobos.css"/>
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
			</head>
			<body>
				<h1>Groteka</h1>
				<xsl:apply-templates/>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="informacje">
		<div id="informacje" class="ramka">
			<h2>Autorzy dokumentu:</h2>
			<xsl:apply-templates select="autor"/>
		</div>
	</xsl:template>

	<xsl:template match="autor">
		<div class="ramka">
			<h3>
				<xsl:apply-templates select="dane_osobowe"/>
			</h3>
			<dl>
				<dt>Numery indeksów:</dt>
				<dd>
					<ul>
						<xsl:apply-templates select="lista_indeksów/indeks"/>
					</ul>
				</dd>
			</dl>
		</div>
	</xsl:template>

	<xsl:template match="dane_osobowe">
		<xsl:value-of select="concat(tytuł_naukowy, ' ', imię, ' ', nazwisko)"/>
	</xsl:template>

	<xsl:template match="indeks">
		<li>
			<xsl:if test="@aktualny='nie'">
				<xsl:attribute name="class">nieaktualny</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
		</li>
	</xsl:template>
	
	<xsl:template match="lista_studiów">
		<div id="lista_studiów" class="ramka">
			<h2>Studia:</h2>
			<xsl:for-each select="studio">
				<xsl:sort select="nazwa"/>
				<div class="ramka">
					<xsl:apply-templates select="."/>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>
	
	<xsl:template match="studio">
		<h3>
			<xsl:value-of select="nazwa"/>
		</h3>
		<dl>
			<dt>Lokalizacja:</dt>
			<dd>
				<xsl:value-of select="lokalizacja/kraj"/>
				<xsl:if test="lokalizacja/stan">
					<xsl:text>, </xsl:text>
					<xsl:value-of select="lokalizacja/stan"/>
				</xsl:if>
			</dd>
			<dt>Opis:</dt>
			<dd>
				<xsl:value-of select="opis"/>
			</dd>
			<dt>Data założenia:</dt>
			<dd>
				<xsl:value-of select="data_założenia"/>
			</dd>
		</dl>
	</xsl:template>
	
	<xsl:template match="biblioteka_gier">
		<div id="lista_gier">
			<xsl:for-each select="gra">
				<xsl:sort select="ocena" data-type="number" order="descending"/>
				<xsl:sort select="nazwa"/>
				<xsl:apply-templates select="."/>
			</xsl:for-each>
		</div>
	</xsl:template>
	
	<xsl:template match="gra">
		<div class="ramka">
			<h2>
				<xsl:value-of select="nazwa"/>
			</h2>
			<div class="ramka zewnętrzna">
				<xsl:apply-templates select="studio"/>
			</div>
			<dl>
				<dt>Studio:</dt>
				<dd>
					<xsl:value-of select="studio/nazwa"/>
				</dd>
				<dt>Gatunki:</dt>
				<dd>
					<ul>
						<xsl:for-each select="lista_tagów/tag">
							<xsl:sort/>
							<xsl:apply-templates select="."/>
						</xsl:for-each>
					</ul>
				</dd>
				<dt>Ocena:</dt>
				<dd>
					<span class="ocena">
						<xsl:attribute name="wartość">
							<xsl:value-of select="ocena"/>
						</xsl:attribute>
						<xsl:choose>
							<xsl:when test="ocena=5">&#xF005;&#xF005;&#xF005;&#xF005;&#xF005;</xsl:when>
							<xsl:when test="ocena=4">&#xF005;&#xF005;&#xF005;&#xF005;&#xF006;</xsl:when>
							<xsl:when test="ocena=3">&#xF005;&#xF005;&#xF005;&#xF006;&#xF006;</xsl:when>
							<xsl:when test="ocena=2">&#xF005;&#xF005;&#xF006;&#xF006;&#xF006;</xsl:when>
							<xsl:when test="ocena=1">&#xF005;&#xF006;&#xF006;&#xF006;&#xF006;</xsl:when>
							<xsl:otherwise>&#xF006;&#xF006;&#xF006;&#xF006;&#xF006;</xsl:otherwise>
						</xsl:choose>
					</span>
					<xsl:value-of select="concat(' (', ocena, '/5)')"/>
				</dd>
				<dt>Cena:</dt>
				<dd>
					<xsl:choose>
						<xsl:when test="cena_w_usd='0,00'">
							Bezpłatna
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat(cena_w_usd, '$')"/>
						</xsl:otherwise>
					</xsl:choose>
				</dd>
				<dt>Data wydania:</dt>
				<dd>
					<xsl:value-of select="data_wydania"/>
				</dd>
				<dt>Opis:</dt>
				<dd>
					<xsl:value-of select="opis_gry"/>
				</dd>
				<dt>Szacunkowa liczba sprzedanych kopii:</dt>
				<dd>
					<xsl:value-of select="liczba_sprzedanych_kopii"/>
				</dd>
			</dl>
		</div>
	</xsl:template>
	
	<xsl:template match="tag">
		<li>
			<xsl:value-of select="."/>
		</li>
	</xsl:template>
	
	<xsl:template match="liczba_gier_studia">
		<tr>
			<td>
				<xsl:value-of select="nazwa_studia"/>
			</td>
			<td class="wartość_liczbowa">
				<xsl:value-of select="liczba"/>
			</td>
			<td class="wartość_liczbowa">
				<xsl:value-of select="statystyki_sprzedaży/liczba_sprzedanych_kopii"/>
			</td>
			<td class="wartość_liczbowa">
				<xsl:value-of select="statystyki_sprzedaży/średnia_liczba_sprzedanych_kopii"/>
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
